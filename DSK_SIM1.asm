.MODEL SMALL
.CODE
ORG 100h
Satu:
	JMP Dua
	Kal1 	DB '=== Surat Izin Mengemudi ==='
		DB 'Firda Mardiaturahman'
		DB 'Sumbawa, 09-09-2002'
		DB 'AB - Perempuan $'
		DB 'DSN Aipaya RT/RW 2/1 Labuhan Aji, Tarano Sumbawa'
	Kal2 	DB 'Pelajar/Mahasiswa'
		DB 'NTB'
		DB '$'
Dua :
	Mov AH,09h
	Lea DX,Kal1
	int 21h
Tiga :
	Mov DX,Offset Kal2 int 21h
	int 20h
End Satu